import { RouterFactory } from '../interfaces/general';
import express, { Response, NextFunction } from 'express';
import { ExtendedRequest } from '../interfaces/express';
import { User, UserRole } from '../models/user.model';
import { Feedback } from '../models/feedback.model';
import roles from '../middleware/roles';
import { body, query, validationResult } from 'express-validator';
import { CacheService } from '../services/CacheService';
import { logger } from '../libs/logger';

export const makeFeedbackRouter: RouterFactory = () => {
  const router = express.Router();

  router.post(
    '',
    roles(['Admin', 'User']),
    [
      body('fromUser').isInt(),
      body('companyName').isString().isLength({ max: 255 }),
      body('toUser').isInt(),
      body('context').isString(),
    ],
    async (req: ExtendedRequest, res: Response, next: NextFunction) => {
      logger.info('[~] Log message from the POST /api/feedback route handler');
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }

      const user = req.user as User;

      const { fromUser, toUser } = req.body;

      if (fromUser === toUser) {
        return res.status(400).json({
          message: 'You cannot post feedback for yourself.',
        });
      }
      if (fromUser !== user.id) {
        return res.status(400).json({
          message: 'You cannot create feedback as someone else.',
        });
      }

      try {
        const newFeedback = await Feedback.create({
          from_user: req.body.fromUser,
          company_name: req.body.companyName,
          to_user: req.body.toUser,
          content: req.body.context,
        });
        await CacheService(user.id);

        return res.status(201).json({
          id: newFeedback.id,
          fromUser: newFeedback.from_user,
          companyName: newFeedback.company_name,
          toUser: newFeedback.to_user,
          context: newFeedback.content,
        });
      } catch (error) {
        res.status(500);
        next(error);
      }
    }
  );

  router.get(
    '/',
    roles(['Admin']),
    query('pageSize').notEmpty(),
    query('page').notEmpty(),
    async (req: ExtendedRequest, res: Response, next: NextFunction) => {
      logger.info('[~] Log message from the GET /api/feedback route handler');
      const pageSize = Number(req.query.pageSize) || 10;
      const page = Number(req.query.page) || 1;
      const user = req.user as User;

      try {
        const { rows: feedbackEntries, count: totalCount } =
          await Feedback.findAndCountAll({
            limit: pageSize,
            offset: (page - 1) * pageSize,
          });

        const formattedFeedbackEntries = feedbackEntries.map((feedback) => ({
          id: feedback.id,
          fromUser: feedback.from_user,
          companyName: feedback.company_name,
          toUser: feedback.to_user,
          context: feedback.content,
        }));

        res.setHeader('X-total-count', totalCount);
        await CacheService(user.id);

        return res.status(200).json(formattedFeedbackEntries);
      } catch (error) {
        res.status(500);
        next(error);
      }
    }
  );

  router.get(
    '/:id',
    async (req: ExtendedRequest, res: Response, next: NextFunction) => {
      logger.info(
        '[~] Log message from the GET /api/feedback/:id route handler'
      );
      try {
        const feedbackId = parseInt(req.params.id);

        if (isNaN(feedbackId)) {
          return res.status(400).json({
            error: 'Invalid ID provided. The ID should be a number.',
          });
        }

        const feedback = await Feedback.findByPk(feedbackId);

        if (!feedback) {
          return res.status(404).json({
            message: 'Feedback with the provided ID does not exist.',
          });
        }

        return res.status(200).json({
          id: feedback.id,
          fromUser: feedback.from_user,
          companyName: feedback.company_name,
          toUser: feedback.to_user,
          context: feedback.content,
        });
      } catch (error) {
        res.status(500);
        next(error);
      }
    }
  );

  router.put(
    '/:id',
    roles(['Admin', 'User']),
    [
      body('fromUser').isInt(),
      body('toUser').isInt(),
      body('context').isString().isLength({ max: 256 }),
      body('companyName').isString().isLength({ max: 256 }),
    ],
    async (req: ExtendedRequest, res: Response, next: NextFunction) => {
      logger.info(
        '[~] Log message from the PUT /api/feedback/:id route handler'
      );
      const validationErrors = validationResult(req);
      if (!validationErrors.isEmpty()) {
        return res.status(400).json({ errors: validationErrors.array() });
      }
      const user = req.user as User;
      const feedbackId = parseInt(req.params.id);

      if (isNaN(feedbackId)) {
        return res.status(400).json({
          error: 'Invalid ID provided. The ID should be a number.',
        });
      }

      try {
        const feedback = await Feedback.findByPk(feedbackId);

        if (!feedback) {
          return res.status(404).json({
            message: 'Feedback not found.',
          });
        }

        if (user.role === UserRole.Admin || feedback.from_user === user.id) {
          const { fromUser, companyName, toUser, context } = req.body;

          await feedback.update({
            from_user: fromUser,
            company_name: companyName,
            to_user: toUser,
            content: context,
          });

          const updatedFeedback = await Feedback.findByPk(feedbackId);
          await CacheService(user.id);

          return res.status(200).json({
            id: updatedFeedback.id,
            fromUser: updatedFeedback.from_user,
            companyName: updatedFeedback.company_name,
            toUser: updatedFeedback.to_user,
            context: updatedFeedback.content,
          });
        } else {
          return res.status(403).json({
            message: 'You have no access to this endpoint',
          });
        }
      } catch (error) {
        res.status(500);
        next(error);
      }
    }
  );

  router.delete(
    '/:id',
    roles(['Admin', 'User']),
    async (req: ExtendedRequest, res: Response, next: NextFunction) => {
      const user = req.user as User;
      const feedbackId = parseInt(req.params.id);
      logger.info(
        '[~] Log message from the DELETE /api/feedback/:id route handler'
      );

      if (isNaN(feedbackId)) {
        return res.status(400).json({
          error: 'Invalid ID provided. The ID should be a number.',
        });
      }

      try {
        const feedback = await Feedback.findByPk(feedbackId);

        if (!feedback) {
          return res.status(404).json({
            message: 'Feedback not found.',
          });
        }
        if (user.role === UserRole.Admin || feedback.from_user === user.id) {
          await feedback.destroy();
          await CacheService(user.id);
          return res
            .status(204)
            .json({ message: 'Feedback has been successfully deleted' });
        } else {
          return res.status(403).json({
            message:
              'Permission denied. You are not authorized to delete this feedback.',
          });
        }
      } catch (error) {
        res.status(500);
        next(error);
      }
    }
  );

  return router;
};
