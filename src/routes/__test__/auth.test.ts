import request from 'supertest';
import { loadApp } from '../../loaders/app';

describe('Authentication Routes', () => {
  it('should register a new user', async () => {
    const app = await loadApp();

    const response = await request(app)
      .post('/api/auth/register')
      .field('firstName', 'Super')
      .field('lastName', 'Test')
      .field('title', 'Software Engineer')
      .field('summary', 'A good software engineer')
      .field('email', 'test1@test.com')
      .field('password', 'test123456')
      .attach('image', 'uploads/image-example.png');

    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty('token');
  });

  it('should fail to register with invalid data', async () => {
    const app = await loadApp();

    const response = await request(app).post('/api/auth/register').send({});

    expect(response.status).toBe(400);
  });

  it('should log in an existing user', async () => {
    const app = await loadApp();

    const response = await request(app)
      .post('/api/auth/login')
      .type('form')
      .send({ email: 'test1@test.com', password: 'test123456' });

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty('token');
  });

  it('should fail to log in with invalid credentials', async () => {
    const app = await loadApp();

    const response = await request(app)
      .post('/api/auth/login')
      .type('form')
      .send({ email: 'wrong@test.com', password: 'testshouldfail12345' });

    expect(response.status).toBe(400);
  });
});
