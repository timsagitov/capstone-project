import request from 'supertest';
import { loadApp } from '../../loaders/app';
import { UserRole } from '../../models/user.model';

describe('Experience Routes', () => {
  it('should create a new experience', async () => {
    const app = await loadApp();
    const user = {
      id: 1,
      role: UserRole.Admin,
    };

    const response = await request(app).post('/api/experience').send({
      user_id: user.id,
      company_name: 'Example Company',
      role: 'Software Engineer',
      startDate: '2023-01-01',
      endDate: '2023-12-31',
      description: 'Worked as a software engineer',
    });

    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty('id');
  });

  it('should fail to create an experience with invalid data', async () => {
    const app = await loadApp();
    const user = {
      id: 1,
      role: UserRole.Admin,
    };

    const response = await request(app).post('/api/experience').send({ user });

    expect(response.status).toBe(400);
  });

  it('should retrieve a list of experiences', async () => {
    const app = await loadApp();
    const response = await request(app).get('/api/experience');

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty('experiences');
    expect(response.body).toHaveProperty('totalCount');
  });

  it('should retrieve a specific experience by ID', async () => {
    const app = await loadApp();
    const experienceId = 1;
    const response = await request(app).get(`/api/experience/${experienceId}`);

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty('id', experienceId);
  });

  it('should update an experience', async () => {
    const app = await loadApp();
    const user = {
      id: 1,
      role: UserRole.Admin,
    };

    const experienceId = 1;

    const response = await request(app)
      .put(`/api/experience/${experienceId}`)
      .send({
        user_id: user.id,
        company_name: 'Updated Company',
        role: 'Updated Role',
        startDate: '2023-01-01',
        endDate: '2023-12-31',
        description: 'Updated description',
      });

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty('id', experienceId);
  });

  it('should fail to update an experience with invalid data', async () => {
    const app = await loadApp();
    const user = {
      id: 1,
      role: UserRole.Admin,
    };

    const experienceId = 1;

    const response = await request(app)
      .put(`/api/experience/${experienceId}`)
      .send({ user });

    expect(response.status).toBe(400);
  });

  it('should delete an experience', async () => {
    const app = await loadApp();
    const experienceId = 1;
    const response = await request(app).delete(
      `/api/experience/${experienceId}`
    );

    expect(response.status).toBe(204);
  });
});
