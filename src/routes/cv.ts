import { RouterFactory } from '../interfaces/general';
import express, { NextFunction, Response } from 'express';
import { ExtendedRequest } from '../interfaces/express';
import { User } from '../models/user.model';
import { Experience } from '../models/experience.model';
import { Project } from '../models/project.model';
import { Feedback } from '../models/feedback.model';
import { redisClient } from '../redis_client';
import { redisMiddleware } from '../middleware/redisMiddleware';
import { logger } from '../libs/logger';

export const makeUserCvRouter: RouterFactory = () => {
  const router = express.Router();

  router.get(
    '/:userId/cv',
    redisMiddleware,
    async (req: ExtendedRequest, res: Response, next: NextFunction) => {
      logger.info(
        'This is a log message from the GET /api/user/:userId/cv route handler'
      );

      try {
        const userId = req.params.userId;
        const cacheKey = `cv:${userId}`;

        const user = await User.findByPk(userId);

        if (!user) {
          return res.status(404).json({ message: 'User not found' });
        }

        const experiences = await Experience.findAll({
          where: { user_id: userId },
        });
        const projects = await Project.findAll({ where: { user_id: userId } });
        const feedbacks = await Feedback.findAll({
          where: { from_user: userId },
          include: [
            {
              model: User,
              as: 'toUser',
              attributes: ['id', 'firstName', 'lastName'],
            },
          ],
        });

        const cv = {
          id: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          title: user.title,
          image: user.image,
          summary: user.summary,
          email: user.email,
          experiences: experiences.map((experience) => ({
            userId: experience.user_id,
            companyName: experience.company_name,
            role: experience.role,
            startDate: experience.startDate,
            endDate: experience.endDate,
            description: experience.description,
          })),
          projects: projects.map((project) => ({
            id: project.id,
            userId: project.user_id,
            image: project.image,
            description: project.description,
          })),
          feedbacks: feedbacks.map((feedback) => ({
            id: feedback.id,
            fromUser: feedback.from_user,
            companyName: feedback.company_name,
            to_user: feedback.to_user,
            context: feedback.content,
          })),
        };

        redisClient.setex(cacheKey, 3600, JSON.stringify(cv));

        res.status(200).json(cv);
      } catch (error) {
        res.status(500);
        next(error);
      }
    }
  );

  return router;
};
