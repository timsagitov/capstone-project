import { RouterFactory } from '../interfaces/general';
import express, { Response, NextFunction } from 'express';
import { ExtendedRequest } from '../interfaces/express';
import { body, query, validationResult } from 'express-validator';
import multer from 'multer';
import { User, UserRole } from '../models/user.model';
import { Project } from '../models/project.model';
import roles from '../middleware/roles';
import { projectsStorage } from '../libs/imgUploader';
import { CacheService } from '../services/CacheService';
import { logger } from '../libs/logger';

const upload = multer({ storage: projectsStorage });

export const makeProjectRouter: RouterFactory = () => {
  const router = express.Router();

  router.post(
    '',
    upload.single('image'),
    roles(['Admin', 'User']),
    [
      body('user_id').isInt(),
      body('image').isString().isLength({ max: 256 }),
      body('description').isString().isLength({ max: 256 }),
    ],
    async (req: ExtendedRequest, res: Response, next: NextFunction) => {
      logger.info('[~] Log message from the route handler');
      const validationErrors = validationResult(req);
      if (!validationErrors.isEmpty()) {
        return res.status(400).json({ errors: validationErrors.array() });
      }
      const user = req.user as User;

      try {
        const { userId, description } = req.body;

        const image = req.file ? req.file.filename : null;

        const project = await Project.create({
          user_id: userId,
          image: image,
          description: description,
        });
        await CacheService(user.id);

        res.status(201).json({
          id: project.id,
          userId: project.user_id,
          image: project.image,
          description: project.description,
        });
      } catch (error) {
        res.status(500);
        next(error);
      }
    }
  );

  router.get(
    '',
    roles(['Admin']),
    query('pageSize').notEmpty(),
    query('page').notEmpty(),
    async (req: ExtendedRequest, res: Response, next: NextFunction) => {
      logger.info('[~] Log message from the GET /api/projects route handler');
      // const currentUser = req.user as User;

      try {
        const pageSize = Number(req.query.pageSize);
        const page = Number(req.query.page);

        if (isNaN(pageSize) || isNaN(page)) {
          return res.status(400).json({
            message:
              'Invalid pageSize or page value. Please provide valid numbers.',
          });
        }

        const offset = (page - 1) * pageSize;

        const { rows: projects, count: totalCount } =
          await Project.findAndCountAll({
            limit: pageSize,
            offset: offset,
          });

        res.setHeader('X-total-count', totalCount);

        res.status(200).json({
          projects,
          totalCount,
        });
      } catch (error) {
        res.status(500);
        next(error);
      }
    }
  );

  router.get(
    '/:id',
    async (req: ExtendedRequest, res: Response, next: NextFunction) => {
      logger.info(
        '[~] Log message from the GET /api/projects/:id route handler'
      );
      try {
        const projectId = parseInt(req.params.id);
        if (isNaN(projectId)) {
          return res.status(400).json({
            error: 'Invalid ID provided. The ID should be a number.',
          });
        }

        const project = await Project.findByPk(projectId);

        if (!project) {
          return res.status(404).json({ message: 'Project not found' });
        }

        res.status(200).json({
          id: project.id,
          userId: project.user_id,
          image: project.image,
          description: project.description,
        });
      } catch (error) {
        res.status(500);
        next(error);
      }
    }
  );

  router.put(
    '/:id',
    roles(['Admin', 'User']),
    [
      body('user_id').isInt(),
      body('image').isString().isLength({ max: 256 }),
      body('description').isString().isLength({ max: 256 }),
    ],
    async (req: ExtendedRequest, res: Response, next: NextFunction) => {
      logger.info(
        '[~] Log message from the PUT /api/projects/:id route handler'
      );
      const validationErrors = validationResult(req);
      if (!validationErrors.isEmpty()) {
        return res.status(400).json({ errors: validationErrors.array() });
      }
      const user = req.user as User;
      const projectId = req.params.id;

      try {
        const project = await Project.findByPk(projectId);

        if (!project) {
          return res
            .status(404)
            .json({ message: 'Project has not been found' });
        }
        if (user.role !== UserRole.Admin && project.user_id !== user.id) {
          return res.status(403).json({
            message: 'You do not have permission to update this project.',
          });
        }

        const { userId, image, description } = req.body;

        await project.update({
          user_id: userId,
          image,
          description,
        });

        await CacheService(user.id);

        res.status(200).json({
          id: project.id,
          userId: project.user_id,
          image: project.image,
          description: project.description,
        });
      } catch (error) {
        res.status(500);
        next(error);
      }
    }
  );

  router.delete(
    '/:id',
    roles(['Admin', 'User']),
    async (req: ExtendedRequest, res: Response, next: NextFunction) => {
      logger.info(
        '[~] Log message from the route DELETE /api/projects/:id handler'
      );
      const user = req.user as User;
      const projectId = parseInt(req.params.id);
      if (isNaN(projectId)) {
        return res.status(400).json({
          error: 'Invalid ID provided. The ID should be a number.',
        });
      }

      try {
        const project = await Project.findByPk(projectId);

        if (!project) {
          return res.status(404).json({ message: 'Project not found' });
        }
        if (user.role !== UserRole.Admin && project.user_id !== user.id) {
          return res.status(403).json({
            message: 'You do not have permission to delete this project.',
          });
        }

        await project.destroy();
        await CacheService(user.id);

        return res.sendStatus(204);
      } catch (error) {
        res.status(500);
        next(error);
      }
    }
  );

  return router;
};
