import { RouterFactory } from '../interfaces/general';
import express, { Response, NextFunction } from 'express';
import { ExtendedRequest } from '../interfaces/express';
import { User, UserRole } from '../models/user.model';
import { Experience } from '../models/experience.model';
import roles from '../middleware/roles';
import { body, query, validationResult } from 'express-validator';
import { logger } from '../libs/logger';
import { CacheService } from '../services/CacheService';

export const makeExperienceRouter: RouterFactory = () => {
  const router = express.Router();

  router.post(
    '',
    roles(['Admin', 'User']),
    [
      body('user_id').isInt(),
      body('company_name').isString().isLength({ max: 255 }),
      body('role').isString().isLength({ max: 255 }),
      body('startDate').isDate(),
      body('endDate').isDate(),
      body('description').isString(),
      body('role').isString(),
    ],
    async (req: ExtendedRequest, res: Response, next: NextFunction) => {
      const user = req.user as User;
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }
      logger.info(
        '[~] Log message from the POST /api/experience route handler'
      );
      const { user_id, company_name, role, startDate, endDate, description } =
        req.body;

      try {
        const newExperience = await Experience.create({
          user_id,
          company_name,
          role,
          startDate,
          endDate,
          description,
        });
        await CacheService(user.id);

        return res.status(201).json(newExperience);
      } catch (error) {
        res.status(500);
        next(error);
      }
    }
  );

  router.get(
    '/',
    roles(['Admin']),
    query('pageSize').notEmpty(),
    query('page').notEmpty(),
    async (req: ExtendedRequest, res: Response, next: NextFunction) => {
      logger.info('[~] Log message from the GET /api/experience route handler');
      const pageSize = Number(req.query.pageSize) || 10;
      const page = Number(req.query.page) || 1;

      try {
        const { rows: experiences, count: totalCount } =
          await Experience.findAndCountAll({
            limit: pageSize,
            offset: (page - 1) * pageSize,
          });

        res.setHeader('X-total-count', totalCount);

        return res.status(200).json({
          experiences,
          totalCount,
        });
      } catch (error) {
        res.status(500);
        next(error);
      }
    }
  );

  router.get(
    '/:id',
    async (req: ExtendedRequest, res: Response, next: NextFunction) => {
      logger.info(
        '[~] Log message from the GET /api/experience/:id route handler'
      );
      try {
        const experienceId = parseInt(req.params.id);

        if (isNaN(experienceId)) {
          return res.status(400).json({
            error: 'Invalid ID provided. The ID should be a number.',
          });
        }

        const experience = await Experience.findByPk(experienceId);

        if (!experience) {
          return res.status(404).json({
            message: 'Experience with the provided ID does not exist.',
          });
        }

        return res.status(200).json({
          id: experience.id,
          userId: experience.user_id,
          companyName: experience.company_name,
          role: experience.role,
          startDate: experience.startDate,
          endDate: experience.endDate,
          description: experience.description,
        });
      } catch (error) {
        res.status(500);
        next(error);
      }
    }
  );

  router.put(
    '/:id',
    [
      body('user_id').isInt().withMessage('User ID must be an integer.'),
      body('company_name')
        .isString()
        .isLength({ max: 256 })
        .withMessage('Company name must be a string, up to 256 characters.'),
      body('role')
        .isString()
        .isLength({ max: 256 })
        .withMessage('Role must be a string, up to 256 characters.'),
      body('startDate')
        .isISO8601()
        .withMessage('Start date must be a valid ISO8601 date.'),
      body('endDate')
        .optional({ nullable: true })
        .isISO8601()
        .withMessage('End date must be a valid ISO8601 date, or null.'),
      body('description')
        .isString()
        .isLength({ max: 256 })
        .withMessage('Description must be a string, up to 256 characters.'),
    ],
    async (req: ExtendedRequest, res: Response, next: NextFunction) => {
      logger.info('[~] Log message from the route handler');

      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }

      const user = req.user as User;
      const experienceId = parseInt(req.params.id);

      if (isNaN(experienceId)) {
        return res.status(400).json({
          error: 'Invalid ID provided. The ID should be a number.',
        });
      }

      try {
        if (user.role === UserRole.Admin) {
          logger.info('[...] Trying to update an experience as an Admin');
          const { userId, companyName, role, startDate, endDate, description } =
            req.body;

          const [updatedCount] = await Experience.update(
            {
              user_id: userId,
              company_name: companyName,
              role,
              startDate,
              endDate,
              description,
            },
            {
              where: {
                id: experienceId,
              },
            }
          );

          if (updatedCount === 0) {
            logger.info('[-] The experience record is not there');
            return res.status(404).json({
              message: "The record of experience hasn't been found",
            });
          }

          const updatedExperience = await Experience.findByPk(experienceId);
          await CacheService(user.id);

          return res.status(200).json({
            id: updatedExperience.id,
            userId: updatedExperience.user_id,
            companyName: updatedExperience.company_name,
            role: updatedExperience.role,
            startDate: updatedExperience.startDate,
            endDate: updatedExperience.endDate,
            description: updatedExperience.description,
          });
        } else {
          const experience = await Experience.findOne({
            where: {
              id: experienceId,
              user_id: user.id,
            },
          });

          if (!experience) {
            return res.status(404).json({
              message: 'Experience record is not found.',
            });
          }

          const { userId, companyName, role, startDate, endDate, description } =
            req.body;

          const [updatedCount] = await Experience.update(
            {
              user_id: userId,
              company_name: companyName,
              role,
              startDate,
              endDate,
              description,
            },
            {
              where: {
                id: experienceId,
              },
            }
          );

          if (updatedCount === 0) {
            return res.status(404).json({
              message: 'Experience not found.',
            });
          }

          const updatedExperience = await Experience.findByPk(experienceId);

          return res.status(200).json({
            id: updatedExperience.id,
            userId: updatedExperience.user_id,
            companyName: updatedExperience.company_name,
            role: updatedExperience.role,
            startDate: updatedExperience.startDate,
            endDate: updatedExperience.endDate,
            description: updatedExperience.description,
          });
        }
      } catch (error) {
        res.status(500);
        next(error);
      }
    }
  );
  router.delete(
    '/:id',
    async (req: ExtendedRequest, res: Response, next: NextFunction) => {
      logger.info(
        '[~] Log message from the DELETE /api/experience/:id route handler'
      );
      const user = req.user as User;
      const experienceId = parseInt(req.params.id);

      if (isNaN(experienceId)) {
        return res.status(400).json({
          error: 'Invalid ID provided. The ID should be a number.',
        });
      }

      try {
        if (user.role !== UserRole.Admin) {
          const experience = await Experience.findOne({
            where: {
              id: experienceId,
              user_id: user.id,
            },
          });

          if (!experience) {
            return res.status(404).json({
              message: 'Experience record is not found.',
            });
          }
        }
        const deletedRows = await Experience.destroy({
          where: {
            id: experienceId,
          },
        });

        if (deletedRows === 0) {
          return res.status(404).json({
            message: 'Experience record is not found.',
          });
        }
        await CacheService(user.id);
        return res.status(204).send('user has been deleted');
      } catch (error) {
        res.status(500);
        next(error);
      }
    }
  );

  return router;
};
