import { Context, RouterFactory } from '../interfaces/general';
import express from 'express';
import multer from 'multer';
import { storage } from '../libs/imgUploader';
import { body } from 'express-validator';

const upload = multer({ storage: storage });

export const makeAuthRouter: RouterFactory = (context: Context) => {
  const router = express.Router();

  router.post(
    '/register',
    upload.single('image'),
    [
      body('firstName').isString().notEmpty(),
      body('lastName').isString().notEmpty(),
      body('title').isString().notEmpty(),
      body('summary').isString().notEmpty(),
      body('email').isEmail().notEmpty(),
      body('password')
        .isLength({ min: 6 })
        .withMessage('The password must cointain more than 5 symbols'),
    ],
    context.services.authService.register
  );

  router.post(
    '/login',
    [
      body('email').isEmail().withMessage('Invalid email format'),
      body('password')
        .isLength({ min: 6 })
        .withMessage('Password must be at least 6 characters'),
    ],
    context.services.authService.login
  );

  return router;
};
