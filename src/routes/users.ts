import { RouterFactory } from '../interfaces/general';
import express, { Response, NextFunction } from 'express';
import { ExtendedRequest } from '../interfaces/express';
import bcrypt from 'bcrypt';
import multer from 'multer';
import { logger } from '../libs/logger';
import { storage } from '../libs/imgUploader';
import { User, UserRole } from '../models/user.model';
import { body, query, validationResult } from 'express-validator';
import roles from '../middleware/roles';
import { CacheService } from '../services/CacheService';

const upload = multer({ storage: storage });

export const makeUserRouter: RouterFactory = () => {
  const router = express.Router();

  router.post(
    '',
    roles(['Admin']),
    upload.single('image'),
    [
      body('firstName').isString().isLength({ max: 255 }),
      body('lastName').isString().isLength({ max: 255 }),
      body('title').isString().isLength({ max: 255 }),
      body('summary').isString().isLength({ max: 255 }),
      body('email').isEmail().isLength({ max: 255 }),
      body('password').isString().isLength({ min: 8, max: 255 }),
      body('role').not().exists(),
    ],
    async (req: ExtendedRequest, res: Response, next: NextFunction) => {
      logger.info('[~] Log message from the POST /api/users route handler');
      const currentUser = req.user as User;
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }
      const { firstName, lastName, title, summary, email, password, role } =
        req.body;

      try {
        const image = req.file ? req.file.filename : null;
        const hashedPassword = await bcrypt.hash(password, 7);

        const user = await User.create({
          firstName,
          lastName,
          title,
          summary,
          email,
          image,
          password: hashedPassword,
          role,
        });
        await CacheService(currentUser.id);

        res.status(201).json({
          id: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          title: user.title,
          summary: user.summary,
          image: user.image,
          email: user.email,
          role: user.role,
        });
      } catch (error) {
        next(error);
      }
    }
  );

  router.get(
    '',
    roles(['Admin']),
    query('pageSize').notEmpty(),
    query('page').notEmpty(),
    async (req: ExtendedRequest, res: Response, next: NextFunction) => {
      logger.info('[~] Log message from the GET /api/users route handler');

      try {
        const pageSize = Number(req.query.pageSize) || 10;
        const page = Number(req.query.page) || 1;

        const users = await User.userPagination(pageSize, page);
        res.status(201).json({
          users,
        });
      } catch (error) {
        res.status(500);
        next(error);
      }
    }
  );
  router.get(
    '/:id',
    async (req: ExtendedRequest, res: Response, next: NextFunction) => {
      logger.info('Log message from the GET /api/users/:id route handler');
      try {
        const userId = parseInt(req.params.id);

        if (isNaN(userId)) {
          return res.status(400).json({
            error: 'Invalid ID provided. The ID should be a number.',
          });
        }
        const user = await User.findByPk(userId);

        if (!user) {
          return res.status(404).json({ message: 'User not found' });
        }

        res.status(200).json({
          id: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          title: user.title,
          summary: user.summary,
          email: user.email,
          password: user.password,
          role: user.role,
        });
      } catch (error) {
        res.status(500);
        next(error);
      }
    }
  );

  router.put(
    '/:id',
    roles(['Admin', 'User']),
    upload.single('image'),
    [
      body('firstName').isString().isLength({ max: 255 }),
      body('lastName').isString().isLength({ max: 255 }),
      body('title').isString().isLength({ max: 255 }),
      body('summary').isString().isLength({ max: 255 }),
      body('email').isEmail().isLength({ max: 255 }),
      body('role').not().exists(),
    ],
    async (req: ExtendedRequest, res: Response, next: NextFunction) => {
      logger.info('Log message from the PUT /api/users/:id route handler');
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }

      const currentUser = req.user as User;
      const userId = req.params.id;
      try {
        const user = await User.findByPk(userId);
        if (!user) {
          return res.status(404).json({ message: 'User not found' });
        }

        if (currentUser.id !== parseInt(userId)) {
          return res.status(403).json({
            message: "You cannot update other person's profile",
          });
        }

        const { firstName, lastName, title, summary, email, password, role } =
          req.body;

        user.firstName = firstName;
        user.lastName = lastName;
        user.title = title;
        user.summary = summary;
        user.email = email;
        user.role = role;

        if (password) {
          user.password = await bcrypt.hash(password, 7);
        }

        if (req.file) {
          const uniqueSuffix =
            Date.now() + '-' + Math.round(Math.random() * 1e9);
          const ext = req.file.originalname.split('.').pop();
          user.image = `${req.file.fieldname}-${uniqueSuffix}.${ext}`;
        }

        await user.save();
        await CacheService(currentUser.id);

        res.status(200).json({
          id: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          title: user.title,
          summary: user.summary,
          email: user.email,
          role: user.role,
        });
      } catch (error) {
        res.status(500);
        next(error);
      }
    }
  );

  router.delete(
    '/:id',
    roles(['Admin', 'User']),
    async (req: ExtendedRequest, res: Response, next: NextFunction) => {
      logger.info('Log message from the DELETE /api/users/:id route handler');
      const currentUser = req.user as User;

      const userId = parseInt(req.params.id);

      if (isNaN(userId)) {
        return res.status(400).json({
          error: 'The id should be a number.',
        });
      }

      try {
        const user = await User.findByPk(userId);
        if (!user) {
          return res.status(404).json({ message: 'User not found' });
        }

        if (currentUser.id !== userId && currentUser.role !== UserRole.Admin) {
          return res.status(403).json({
            message:
              'Account cannot be deleted due to lack of your permissions',
          });
        }

        await user.destroy();
        await CacheService(currentUser.id);

        return res.sendStatus(204).json({
          message: 'Successfully deleted the user',
        });
      } catch (error) {
        res.status(500);
        next(error);
      }
    }
  );

  return router;
};
