import { Loader } from '../interfaces/general';
import { ExtendedRequest } from '../interfaces/express';
import { logger } from '../libs/logger';

export const loadMiddlewares: Loader = (app, context) => {
  // LOG REQUESTS WITH REQUEST'S ID
  app.use((req: ExtendedRequest, res, next) => {
    logger.info(
      `${req.id} ::: ${req.method} ${
        req.protocol + '://' + req.hostname + req.originalUrl
      }`
    );
    return next();
  });
};
