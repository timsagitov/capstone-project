import { Context, Models } from '../interfaces/general';
import { AuthService } from '../services/auth.service';

export const loadContext = async (models: Models): Promise<Context> => {
  return {
    services: {
      authService: new AuthService(),
    },
    models: models,
  };
};
