import express from 'express';
import { Context } from '../interfaces/general';
import passport from 'passport';
import { makeAuthRouter } from '../routes/auth';
import { makeUserRouter } from '../routes/users';
import { makeExperienceRouter } from '../routes/experience';
import { makeFeedbackRouter } from '../routes/feedback';
import { makeProjectRouter } from '../routes/projects';
import { makeUserCvRouter } from '../routes/cv';
import { errorHandler } from '../middleware/errorHandler';

export const loadRoutes = (app: express.Router, context: Context) => {
  app.use('/api/auth', makeAuthRouter(context));
  app.use(
    '/api/users',
    passport.authenticate('jwt', { session: false }),
    makeUserRouter(context)
  );
  app.use(
    '/api/experience',
    passport.authenticate('jwt', { session: false }),
    makeExperienceRouter(context)
  );
  app.use(
    '/api/feedback',
    passport.authenticate('jwt', { session: false }),
    makeFeedbackRouter(context)
  );
  app.use(
    '/api/projects',
    passport.authenticate('jwt', { session: false }),
    makeProjectRouter(context)
  );
  app.use(
    '/api/user',
    passport.authenticate('jwt', { session: false }),
    makeUserCvRouter(context)
  );
  app.use(errorHandler);
};
