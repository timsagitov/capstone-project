import { loadMiddlewares } from './middlewares';
import { loadRoutes } from './routes';
import express from 'express';
import { loadContext } from './context';
import { loadModels } from './models';
import { loadSequelize } from './sequelize';
import { config } from '../config';
import { loadPassport } from './passport';
import expressRequestId from 'express-request-id';

export const loadApp = async () => {
  const app = express();
  const sequelize = loadSequelize(config);

  const models = loadModels(sequelize);

  const context = await loadContext(models);
  app.use(express.json());
  app.use(expressRequestId());

  loadPassport(app, context);
  loadMiddlewares(app, context);
  loadRoutes(app, context);

  return app;
};
