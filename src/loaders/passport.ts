import { Context, Loader } from '../interfaces/general';
import { passportStrategies } from '../libs/passportStrategies';
import passport from 'passport';

export const loadPassport: Loader = (app, context: Context) => {
  //   app.set('trust proxy', 1);
  app.use(passport.initialize());
  passportStrategies(context);
};
