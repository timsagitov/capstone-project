import { Request, Response, NextFunction } from 'express';

function roles(allowedRoles: string[]) {
  return (req: Request, res: Response, next: NextFunction) => {
    const user = req.user as { role: string };

    if (user && allowedRoles.includes(user.role)) {
      next();
    } else {
      res.status(403).json({ error: 'Forbidden' });
    }
  };
}

export default roles;
