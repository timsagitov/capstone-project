import { Response } from 'express';
import { ExtendedRequest } from '../interfaces/express';
import { redisClient } from '../redis_client';
import { logger } from '../libs/logger';

export const redisMiddleware = (
  req: ExtendedRequest,
  res: Response,
  next: any
) => {
  const userId = req.params.userId;
  const cacheKey = `cv:${userId}`;

  redisClient.get(cacheKey, (err, data) => {
    if (err) {
      logger.error('[-] Error getting data from Redis cache:', err);
      next();
    }

    if (data) {
      logger.info('[+] Successfully retrieved the data from Redis cache');
      res.status(200).json(JSON.parse(data));
    } else {
      next();
    }
  });
};
