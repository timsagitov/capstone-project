import { NextFunction, Response } from 'express';
import multer from 'multer';
import { ExtendedRequest } from '../interfaces/express';
import ImageService from '../services/image.service';
import { logger } from '../libs/logger';
