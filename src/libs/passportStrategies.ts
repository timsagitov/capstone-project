import passport from 'passport';
import passportLocal from 'passport-local';
import passportJWT from 'passport-jwt';

import { Context } from '../interfaces/general';
import { User } from '../models/user.model';
import { config } from '../config';
import { logger } from './logger';

const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;

export function passportStrategies(context: Context) {
  passport.use(
    new passportLocal.Strategy(
      {
        usernameField: 'email',
        passwordField: 'password',
      },
      async (email, password, done) => {
        try {
          const user = await context.models.user.findOne({ where: { email } });

          if (!user) {
            logger.debug(`User with an email --> ${email} doesn't exist`);
            return done(null, false, {
              message: 'User not found',
            });
          }

          const passwordChecked = await user.comparePassword(password);

          if (!passwordChecked) {
            return done(null, null, {
              message: 'Invalid username or password',
            });
          }

          return done(null, user, {
            message: 'User has sucessfully logged in',
          });
        } catch (error) {
          return done(error);
        }
      }
    )
  );

  passport.use(
    new JWTStrategy(
      {
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey: config.auth.secret,
      },
      async (
        jwtPayload: { id: number; email: string },
        done: CallableFunction
      ) => {
        return context.models.user
          .findByPk(jwtPayload.id)
          .then((user: User) => {
            done(null, user);
          })
          .catch((err: Error) => {
            logger.error(err);
            done(err);
          });
      }
    )
  );
}
