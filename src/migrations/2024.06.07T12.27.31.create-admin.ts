import { Sequelize } from 'sequelize';
import { hash } from 'bcrypt';
import { MigrationFn } from 'umzug';

export const up: MigrationFn<Sequelize> = async ({ context }) => {
  const q = context.getQueryInterface();

  const password = await hash('test-admin', 10);

  await q.bulkInsert('users', [
    {
      first_name: 'admin',
      last_name: 'Admin',
      image: 'uploads/admin',
      title: 'Admin',
      summary: 'Admin summary',
      role: 'Admin',
      email: 'admin@socialnetwork.com',
      password,
      created_at: new Date(),
      updated_at: new Date(),
    },
  ]);
};

export const down: MigrationFn<Sequelize> = async ({ context }) => {
  const q = context.getQueryInterface();

  await q.bulkDelete('users', { role: 'Admin' });
};
