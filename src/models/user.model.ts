import { DataTypes, Model, Optional, Sequelize } from 'sequelize';
import { Models } from '../interfaces/general';
import { compare } from 'bcrypt';

export enum UserRole {
  Admin = 'Admin',
  User = 'User',
}

interface UserAttributes {
  id: number;
  firstName: string;
  lastName: string;
  image: string;
  title: string;
  summary: string;
  role: UserRole;
  email: string;
  password: string;
}

export class User
  extends Model<UserAttributes, Optional<UserAttributes, 'id'>>
  implements UserAttributes
{
  id: number;
  firstName: string;
  lastName: string;
  image: string;
  title: string;
  summary: string;
  role: UserRole;
  email: string;
  password: string;

  readonly createdAt: Date;
  readonly updatedAt: Date;

  static defineSchema(sequelize: Sequelize) {
    User.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        firstName: {
          field: 'first_name',
          type: new DataTypes.STRING(128),
          allowNull: false,
        },
        lastName: {
          field: 'last_name',
          type: new DataTypes.STRING(128),
          allowNull: false,
        },
        image: {
          type: new DataTypes.STRING(256),
          allowNull: false,
        },
        title: {
          type: new DataTypes.STRING(256),
          allowNull: false,
        },
        summary: {
          type: new DataTypes.STRING(256),
          allowNull: false,
        },
        role: {
          type: new DataTypes.STRING(50),
          allowNull: false,
        },
        email: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        password: {
          type: DataTypes.STRING,
          allowNull: false,
        },
      },
      {
        tableName: 'users',
        underscored: true,
        sequelize,
      }
    );
  }

  static associate(models: Models, sequelize: Sequelize) {
    User.hasMany(models.experience, {
      foreignKey: 'user_id',
    });

    User.hasMany(models.project, {
      foreignKey: 'user_id',
    });

    User.hasMany(models.feedback, {
      foreignKey: 'from_user',
    });

    User.hasMany(models.feedback, {
      foreignKey: 'to_user',
    });
  }

  async comparePassword(candidate: string): Promise<boolean> {
    return compare(candidate, this.password);
  }

  static async userPagination(pageSize: number, page: number): Promise<User[]> {
    try {
      if (pageSize <= 0 || page <= 0) {
        throw new Error(
          'Invalid pageSize or page value. Please provide valid positive numbers.'
        );
      }

      const offset = (page - 1) * pageSize;
      return await this.findAll({
        limit: pageSize,
        offset,
      });
    } catch (error) {
      throw new Error(`Error while paginating users: ${error.message}`);
    }
  }
}
