import { redisClient } from '../redis_client';
import { logger } from '../libs/logger';

export const CacheService = (userId: number): void => {
  const cacheKey = `cv:${userId}`;

  redisClient.del(cacheKey, (err: Error | null, response: number) => {
    if (err) {
      logger.error(err);
    } else {
      logger.info(`[+] Cleared Redis Cache for the key: ${cacheKey}`);
    }
  });
};
