import fs from 'fs';
import sharp from 'sharp';
import path from 'path';

import { uuid } from 'uuidv4';
import { promisify } from 'util';

const fsUnlink = promisify(fs.unlink);

import { logger } from '../libs/logger';

export default class ImageService {
  constructor(public directory: string) {
    logger.debug(`Image storage started at: ${directory}`);
    logger.debug(
      `Example filepath for image (local): ${this.filepath(
        ImageService.filename('.png')
      )}`
    );

    logger.debug(
      `Example filepath for image (in API): ${this.filepathOuter(
        ImageService.filename('.png')
      )}`
    );
  }

  async store(buffer: Buffer, ext: string) {
    const filename = ImageService.filename(ext);
    const filepath = this.filepath(filename);
    try {
      await sharp(buffer)
        .resize(300, 300, {
          fit: sharp.fit.inside,
          withoutEnlargement: true,
        })
        .toFile(filepath);
    } catch (err) {
      logger.error(`Something went wrong while processing file ${filepath}`);
    }

    return filename;
  }

  async delete(filename: string) {
    return fsUnlink(this.filepath(filename));
  }

  static filename(ext: string) {
    return `${uuid()}${ext}`;
  }

  filepathOuter(filename: string) {
    return `${this.directory}/${filename}`;
  }

  filepath(filename: string) {
    return path.resolve(
      __dirname,
      path.join('..', '..', 'public', this.directory, filename)
    );
  }
}

module.exports = ImageService;
