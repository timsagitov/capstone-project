"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.loadModels = void 0;
const user_model_1 = require("../models/user.model");
const loadModels = (sequelize) => {
    const models = {
        user: user_model_1.User,
    };
    for (const model of Object.values(models)) {
        model.defineSchema(sequelize);
    }
    for (const model of Object.values(models)) {
        model.associate(models, sequelize);
    }
    return models;
};
exports.loadModels = loadModels;
//# sourceMappingURL=models.js.map